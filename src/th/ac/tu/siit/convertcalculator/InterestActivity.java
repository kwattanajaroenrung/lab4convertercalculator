package th.ac.tu.siit.convertcalculator;

import java.util.Locale;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class InterestActivity extends Activity implements OnClickListener{
	
	float outRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button c1 = (Button)findViewById(R.id.btnCal);
		c1.setOnClickListener(this);
		
		Button c2 = (Button)findViewById(R.id.btnSetting);
		c2.setOnClickListener(this);
		
		
		
		
		TextView textView4 = (TextView)findViewById(R.id.textView4);
		outRate = Float.parseFloat(textView4.getText().toString());

		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
         int id = v.getId();
		
		if (id == R.id.btnCal) {
			EditText amounts = (EditText)findViewById(R.id.editText1);
			EditText years = (EditText)findViewById(R.id.editText2);
			TextView all = (TextView)findViewById(R.id.calculate);
			String res = "";
			
				float amount = Float.parseFloat(amounts.getText().toString());
				float year = Float.parseFloat(years.getText().toString());
				double all1 = amount* Math.pow((1+(outRate/100)),year);
				res = String.format(Locale.getDefault(), "%.2f",all1 );
			
			all.setText(res);
		}
		
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("outRate", outRate);
			startActivityForResult(i, 999);//request code = identify the return value
		}
	}

	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 999 && resultCode == RESULT_OK) {
			outRate = data.getFloatExtra("outRate", 10.0f);
			TextView textView4 = (TextView)findViewById(R.id.textView4);
			textView4.setText(String.format(Locale.getDefault(), "%.2f", outRate));
		}
	}

}
